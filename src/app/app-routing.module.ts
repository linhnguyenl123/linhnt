import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoryListComponent } from './components/pages/category-list/category-list.component';
import { ProductManagementComponent } from './components/pages/product-management/product-management.component';
import { PricePolicyComponent } from './components/pages/price-policy/price-policy.component';
import { ProcessManagementComponent } from './components/pages/process-management/process-management.component';
import { ReportManagementComponent } from './components/pages/report-management/report-management.component';
import { AddProductCategoryComponent } from './components/pages/category-list/add-product-category/add-product-category.component';

const routes: Routes = [
  {path: '', component: CategoryListComponent},
  {path: 'category', component: CategoryListComponent},
  {path: 'category/:add-product-category', component: AddProductCategoryComponent},
  {path: 'product', component: ProductManagementComponent},
  {path: 'price', component: PricePolicyComponent},
  {path: 'process', component: ProcessManagementComponent},
  {path: 'report', component: ReportManagementComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
