import { Component, OnInit } from '@angular/core';
import { ProcessService } from 'src/app/sharing/service/process.service';

@Component({
  selector: 'app-process-management',
  templateUrl: './process-management.component.html',
  styleUrls: ['./process-management.component.css']
})
export class ProcessManagementComponent implements OnInit {


  data = [];

  constructor(private processService: ProcessService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.data = this.processService.getData();
  }
}
