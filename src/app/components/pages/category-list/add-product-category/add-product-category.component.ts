import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder } from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-add-product-category',
  templateUrl: './add-product-category.component.html',
  styleUrls: ['./add-product-category.component.css']
})
export class AddProductCategoryComponent implements OnInit {
  propertyForm: FormGroup;
  items : FormArray;

  constructor(private formBuilder : FormBuilder, private location: Location) { 
    this.propertyForm = new FormGroup({
      items: new FormArray([])
    });
  }
///ghg
  ngOnInit(): void {

  }

  addProperty(){
    const addItem = this.propertyForm.controls.items as FormArray;
    addItem.push(this.formBuilder.group({
      name:'',
      typeProperty: '',
      constraintProperty: ''
    }))
  }

  swapProperty(i){
    this.items = this.propertyForm.get('items') as FormArray;
    if(i != 0){
      const temp = Object.assign({},(<FormArray> this.propertyForm.controls['items']).at(i-1).value);
      (<FormArray> this.propertyForm.controls['items']).at(i - 1).setValue((<FormArray> this.propertyForm.controls['items']).at(i).value);
      (<FormArray> this.propertyForm.controls['items']).at(i).setValue(temp);
    }
    console.log("done");
  }

  moveUp(index: number) {
    if (index > 0) {
          const control = this.propertyForm.get('items') as FormArray;
          const extras = control.value;
          const newExtras = this.swap(extras, index - 1, index); 
          control.setValue(newExtras);
    }
  }

  moveDown(index: number) {
    const control = this.propertyForm.get('items') as FormArray;
    const extras = control.value;
    if (index < extras.length - 1) {
      const newExtras = this.swap(extras, index, index + 1); 
      control.setValue(newExtras);
    }
  }

  removeItem(index: number) {
    const control = this.propertyForm.get('items') as FormArray;
    control.removeAt(index);
  }

  swap(arr: any[], index1: number, index2: number): any[] {
    arr = [...arr];
    const temp = arr[index1];
    arr[index1] = arr[index2];
    arr[index2] = temp;
    return arr;
  }

  backClicked(){
    this.location.back();
  }
}
