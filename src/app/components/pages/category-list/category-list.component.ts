import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/sharing/service/category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  data = [
  ]

  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.getData();
  }

/**
 * //TODO: comment getData
 * get data
 * return data
 */
  getData(){
    this.data = this.categoryService.getData();
  }

}
