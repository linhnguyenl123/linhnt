import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricePolicyComponent } from './price-policy.component';

describe('PricePolicyComponent', () => {
  let component: PricePolicyComponent;
  let fixture: ComponentFixture<PricePolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricePolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricePolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
