import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { CategoryListComponent } from './components/pages/category-list/category-list.component';
import { ProductManagementComponent } from './components/pages/product-management/product-management.component';
import { PricePolicyComponent } from './components/pages/price-policy/price-policy.component';
import { ProcessManagementComponent } from './components/pages/process-management/process-management.component';
import { ReportManagementComponent } from './components/pages/report-management/report-management.component';
import { AddProductCategoryComponent } from './components/pages/category-list/add-product-category/add-product-category.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    CategoryListComponent,
    ProductManagementComponent,
    PricePolicyComponent,
    ProcessManagementComponent,
    ReportManagementComponent,
    AddProductCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
