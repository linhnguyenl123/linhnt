import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProcessService {

  constructor(private httpClient : HttpClient) { }

  getData(){
    var data = [
      {
        name : "Quy trình tích hợp BCCS",
        invent: "payment-success"
      },
      {
        name : "Quy trình tích hợp Telerad",
        invent: "consume-item"
      }
    ]
    return data;
  }
}
