import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient: HttpClient) { }

  // getCategory(): Observable<any> {
  //   let url = "";
  //   return this.httpClient.get(url);
  // }

  getData(){
    var data = [
      {
        title: "Dịch vụ y tế",
        content: "125 sản phẩm"
      },
      {
        title: "Dịch vụ phần mềm",
        content: "35 sản phẩm"
      },
      {
        title: "Khóa học",
        content: "1.425 sản phẩm"
      }
    ]
    return data;
  }


}
